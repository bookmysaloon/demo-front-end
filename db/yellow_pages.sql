-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 15, 2016 at 10:46 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `yellow_pages`
--

-- --------------------------------------------------------

--
-- Table structure for table `yp_business_categories`
--

CREATE TABLE IF NOT EXISTS `yp_business_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_category_name` varchar(500) NOT NULL,
  `created_by` varchar(500) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yp_business_details`
--

CREATE TABLE IF NOT EXISTS `yp_business_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_name` varchar(500) NOT NULL,
  `open_time` varchar(500) NOT NULL,
  `closing_time` varchar(500) NOT NULL,
  `address` longtext NOT NULL,
  `phone_number` int(11) NOT NULL,
  `website_link` varchar(500) NOT NULL,
  `email_business` varchar(500) NOT NULL,
  `social_links` varchar(500) NOT NULL,
  `regular_working_hours` varchar(500) NOT NULL,
  `general_info` varchar(500) NOT NULL,
  `products` varchar(5) NOT NULL,
  `brands` varchar(500) NOT NULL,
  `payment_method` varchar(500) NOT NULL,
  `location` varchar(500) NOT NULL,
  `neighborhoods` varchar(500) NOT NULL,
  `other_links` varchar(500) NOT NULL,
  `other_email` varchar(500) NOT NULL,
  `categories` varchar(500) NOT NULL,
  `map_latitude` varchar(500) NOT NULL,
  `map_longitude` varchar(500) NOT NULL,
  `zip_code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yp_business_gallery`
--

CREATE TABLE IF NOT EXISTS `yp_business_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_details_id` int(11) NOT NULL,
  `image_path` varchar(500) NOT NULL,
  `ceated_by` varchar(500) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yp_business_information`
--

CREATE TABLE IF NOT EXISTS `yp_business_information` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_name` varchar(500) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `first_name` varchar(500) NOT NULL,
  `last_name` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `categories` varchar(500) NOT NULL,
  `street_address` longtext NOT NULL,
  `city` varchar(500) NOT NULL,
  `state` varchar(500) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `year_established` int(11) NOT NULL,
  `created_by` varchar(500) NOT NULL,
  `created_date` date NOT NULL,
  `status` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `yp_business_information`
--

INSERT INTO `yp_business_information` (`id`, `business_name`, `phone_number`, `first_name`, `last_name`, `email`, `categories`, `street_address`, `city`, `state`, `zip_code`, `year_established`, `created_by`, `created_date`, `status`) VALUES
(1, 'Shoe Shop', 1223455678, 'ammu', 'n', 'ammu@gmail.com', 'shoes', 'Kakanad,Ernakulam,Kerala', 'Kakanad', 'Kerala', 682021, 1993, '1', '2016-02-13', '1'),
(2, 'Textile shop', 2147483647, 'Raju', 'R', 'raju@gmail.com', 'clothing', 'Vytila,Ernakulam,Kerala', 'Vytila', 'Kerala', 682019, 1990, '2', '2016-02-13', '1'),
(3, 'Furniture Shop', 2147483647, 'Nivin', 'poly', 'nivin@gmail.com', 'Furniture ', 'Kochi,Ernakulam,Kerala', 'Kochi', 'Kerala', 682001, 1994, '3', '2016-02-13', '1'),
(4, 'Restaurant', 1223455678, 'ammu', 'n', 'ammu@gmail.com', 'Restaurant', 'Chengalam,Kottayam,Kerala', 'Kottyam', 'Kerala', 686022, 1999, '1', '2016-02-13', '1'),
(5, 'Electronics Shop', 1223455678, 'ammu', 'n', 'ammu@gmail.com', 'Home electronics ', 'kumarakom,Kottayam,Kerala', 'kumarakom', 'Kerala', 686563, 1995, '1', '2016-02-13', '1'),
(6, 'Restaurant', 2147483647, 'Raju', 'R', 'raju@gmail.com', 'Restaurant', 'Newyork,USA', 'Newyork', 'Newyork', 10005, 2000, '2', '2016-02-13', '1'),
(7, 'Coffee Shop', 2147483647, 'Raju', 'R', 'raju@gmail.com', 'Coffee and tea ', 'kumarakom,Kottayam,Kerala', 'kumarakom', 'Kerala', 686563, 2001, '2', '2016-02-13', '1'),
(8, 'Web Designing', 2147483647, 'Nivin', 'poly', 'nivin@gmail.com', 'Designing', 'Newyork,USA', 'Newyork', 'Newyork', 10005, 1995, '3', '2016-02-13', '1'),
(9, 'Textile Shop', 2147483647, 'Nivin', 'poly', 'nivin@gmail.com', 'Clothing', 'kumarakom,Kottayam,Kerala', 'kumarakom', 'Kerala', 686563, 2010, '3', '2016-02-13', '1');

-- --------------------------------------------------------

--
-- Table structure for table `yp_business_users`
--

CREATE TABLE IF NOT EXISTS `yp_business_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(500) NOT NULL,
  `last_name` varchar(500) NOT NULL,
  `username` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `city` varchar(500) NOT NULL,
  `state` varchar(500) NOT NULL,
  `country` varchar(500) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `business_name` varchar(500) NOT NULL,
  `website_name` varchar(500) NOT NULL,
  `categories` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yp_favourite`
--

CREATE TABLE IF NOT EXISTS `yp_favourite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `yp_favourite`
--

INSERT INTO `yp_favourite` (`id`, `business_id`, `user_id`, `created_date`) VALUES
(4, 3, 1, '2016-02-15'),
(6, 7, 1, '2016-02-15'),
(8, 1, 1, '2016-02-15');

-- --------------------------------------------------------

--
-- Table structure for table `yp_ratings`
--

CREATE TABLE IF NOT EXISTS `yp_ratings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_details_id` int(11) NOT NULL,
  `rating` varchar(500) NOT NULL,
  `created_by` varchar(500) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yp_reviews`
--

CREATE TABLE IF NOT EXISTS `yp_reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `business_details_id` int(11) NOT NULL,
  `comments` varchar(500) NOT NULL,
  `created_by` varchar(500) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `yp_users`
--

CREATE TABLE IF NOT EXISTS `yp_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(500) NOT NULL,
  `last_name` varchar(500) NOT NULL,
  `username` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `phone_number` int(11) NOT NULL,
  `city` varchar(500) NOT NULL,
  `state` varchar(500) NOT NULL,
  `country` varchar(500) NOT NULL,
  `zip_code` int(11) NOT NULL,
  `created_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `yp_users`
--

INSERT INTO `yp_users` (`id`, `first_name`, `last_name`, `username`, `email`, `password`, `phone_number`, `city`, `state`, `country`, `zip_code`, `created_date`) VALUES
(1, 'ammu', 'n', 'ammumol', 'ammu@gmail.com', '1234', 1223455678, 'Kakanad', 'Kerala', 'India', 682021, '2016-02-13'),
(2, 'Raju', 'R', 'rajas', 'raju@gmail.com', '1234', 2147483647, 'Kakanad', 'Kerala', 'India', 682021, '2016-02-13'),
(3, 'Nivin', 'poly', 'naveen', 'nivin@gmail.com', '1234', 2147483647, 'Vytila', 'Kerala', 'India', 686022, '2016-02-13');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
