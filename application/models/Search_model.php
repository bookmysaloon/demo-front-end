<?php
class Search_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();

           
        }
  
  //get search datas
   function get_search($data) {
     
     if($data)
     {
     $businessname=$data['business_name'];
     $city=$data['place'];
    
       $this->db->like('business_name',$businessname);
       $this->db->like('city',$city);
       $query=$this->db->get('yp_business_information');
       $query=$query->result();

       return $query;
     }
     else
     {

      $query=$this->db->get('yp_business_information');
      $query=$query->result();

       return $query;
     }


   }

  //full search details shows 
   function get_search_result($business_id) {
     
     $sess_id=$this->session->userdata('userdatas');
     $fav_select = "";
     if($sess_id)
       {
       $user_id = $sess_id['id'];
       $fav_select = ",fav.id as f_id";
       }
       $this->db->select('business.*'.$fav_select);
       $this->db->from('yp_business_information business');
       if($sess_id)
       {
       $this->db->join('yp_favourite fav', "fav.business_id = business.id and fav.user_id = $user_id",'left');
       
       }
     
       $query=$this->db->where('business.id', $business_id);
       $query=$this->db->get();
       $query=$query->result();

       return $query;

   }

  //add favourite   
   function addfavourite($business_id,$user_id){

       $value=array(

                   'business_id'=>$business_id,
                   'user_id'=>$user_id,
                   'created_date'=>date('y-m-d'),
                   
                      );

       $this->db->from('yp_favourite');
       $query=$this->db->where('user_id',$user_id);
       $query=$this->db->where('business_id',$business_id);
       $query=$this->db->count_all_results();
        
        if($query=='0')
        {
        
        $result = $this->db->insert('yp_favourite', $value); 
        
        return $result;
        }
        else
        {
           $query=$this->db->where('user_id',$user_id);
           $query=$this->db->where('business_id',$business_id);
           $result=$this->db->delete('yp_favourite');

           return $result;
        }
    }

  //get reviews  
    function get_reviews($business_id){

     
      $this->db->select('reviews.*,users.id as u_id,users.first_name');
      $this->db->from('yp_reviews reviews');
      $this->db->join('yp_users users', 'users.id = reviews.user_id','left');
      $this->db->where('reviews.business_id',$business_id);
      $this->db->order_by('reviews.created_date', "desc");
      $query=$this->db->get();
      $query=$query->result();

        return $query;
      
    }     
   

}
?>