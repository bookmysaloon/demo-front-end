<?php
class Business_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
        }

  //add business 
    function add_business($data){
     
        $add_business = $this->db->insert('yp_business_information', $data); 
        
        return $add_business;
          
    } 
  
  //show business details 
    function details_business($user_id)
    {  
     
       $this->db->select('business.*,fav.business_id,fav.user_id,fav.id as f_id');
       $this->db->from('yp_business_information business');
       $this->db->join('yp_favourite fav', 'fav.business_id = business.id and fav.user_id = business.created_by','left');
       $query=$this->db->where('business.created_by',$user_id);
       $query=$this->db->where('business.status','1');
       $query=$query->get();
       $query_business=$query->result();

       return $query_business;

        
    }

  //get business details  
    function business_update($id,$user_id)
    {   
       $query=$this->db->where('created_by',$user_id);      
       $query=$this->db->where('id',$id);
       $query=$this->db->where('status','1');
       $query=$this->db->get('yp_business_information'); 
       $query_result=$query->row();

       
         return $query_result;
        
         
        
    }

  //update business details   
    function business_update_result($value,$id)
    {
      $user_id=$value['created_by'];

      $query=$this->db->where('created_by',$user_id);  
      $query=$this->db->where('id', $id);
      $query=$this->db->where('status','1');
      $query=$this->db->update('yp_business_information', $value); 
        
       
        return $query;
         
    }
  //delete business details(update status as  0).  
    function business_delete($id)
    {

      $value=array(
                   'status'=>'0'
                  );
      $query=$this->db->where('id', $id);
      $query=$this->db->update('yp_business_information', $value); 
      
        return $query;
    }
    

}
?>