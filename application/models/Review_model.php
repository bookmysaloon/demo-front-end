<?php
class Review_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
        }

  //add rating and review 
    function addreview($data){

    	//var_dump($data) ;exit; 
    	$this->db->where('business_id',$data['business_id']);
    	$this->db->where('user_id',$data['user_id']);
    	$query=$this->db->get('yp_reviews');
    	$get_details=$query->row();

    	if($get_details==0)
    	{   
          $query = $this->db->insert('yp_reviews', $data); 
        
           return $query;
        }
        else
        {  

           $this->db->where('business_id',$data['business_id']);
    	     $this->db->where('user_id',$data['user_id']);
           $query = $this->db->update('yp_reviews', $data); 
        
           return $query;
        }	
          
    }

   //get review details 
    function get_review_details($business_id,$user_id){

    	$this->db->where('business_id',$business_id);
    	$this->db->where('user_id',$user_id);
      $query=$this->db->get('yp_reviews');
    	$query=$query->row();

        //return $query;
        
        if($query)
        {
           return $query;
        }
        else
        {
          return "Novalue";
        }  

    }      

}
?>