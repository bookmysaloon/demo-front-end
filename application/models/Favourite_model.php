<?php
class Favourite_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
        }

  //add favourite 
    function add_favourite($business_id,$u_id){  
       
       $value=array(

                   'business_id'=>$business_id,
                   'user_id'=>$u_id,
                   'created_date'=>date('y-m-d'),
                   
                      );

       $this->db->from('yp_favourite');
       $query=$this->db->where('user_id',$u_id);
       $query=$this->db->where('business_id',$business_id);
       $query=$this->db->count_all_results();
        
        if($query=='0')
        {
        
        $result = $this->db->insert('yp_favourite', $value); 
        
        return $result;
        }
        else
        {
           $query=$this->db->where('user_id',$u_id);
           $query=$this->db->where('business_id',$business_id);
           $result=$this->db->delete('yp_favourite');

           return $result;
        }
    }

  //favourite list  
    function favourite_list($user_id){ 
     
       $this->db->select('fav.*,business.id as b_id,business.business_name,business.categories');
       $this->db->from('yp_favourite as fav');
       $this->db->join('yp_business_information as business', 'business.id = fav.business_id');
       $query=$this->db->where('fav.user_id',$user_id);
       $query=$query->get();
       $query_fav_list=$query->result();

       return $query_fav_list;
    }

   //unfavourite  
    function delete_favourite($fav_id,$user_id){  
       
       $query=$this->db->where('user_id',$user_id);
       $query=$this->db->where('id',$fav_id);
       $result=$this->db->delete('yp_favourite');

       return $result;
        
    }
   
   

}
?>