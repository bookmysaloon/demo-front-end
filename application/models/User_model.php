<?php
class User_model extends CI_Model {

        public function __construct()
        {
                parent::__construct();
        }
  //user registration      

    function user_registration($data) {


      $username = $data['username'];
      $email = $data['email'];

      $this->db->where('username', $username);
      $query= $this->db->get('yp_users');
  
     
       if ($query->num_rows()==0)
        {
      
           $this->db->where('email', $email);
           $query1= $this->db->get('yp_users');
  
           if($query1->num_rows() ==0)
            {
               $reg = $this->db->insert('yp_users', $data); 
                return "Success";

            }
            else
            {
               return "EmailExist";
            }
          
        }
        else 
        {
         return "UsernameExist";
             
        }
          
          
    }

  //user login  
    function login($data){
       
        $email=$data['email'];
        $password=$data['password'];
                 
         $this->db->where('email',$email);
         $this->db->where('password',$password);
         $query=$this->db->get('yp_users');
         $query_value=$query->row();

         return $query_value;
     
    }
    

}
?>