<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends CI_Controller {

	  public function __construct(){
		
		parent::__construct();
		
		$this->load->model('Search_model','search');
	
		}

//search
	public function search(){

     if($_POST)
     {

	   
       $data=$_POST;
       $template['result']=$this->search->get_search($data);
      
        if($template['result'])
        {
	      $this->load->view('Search/search_list',$template);
	    }
	    else
        {
          $this->session->set_flashdata('msg', array('message' => 'No Result','class' => 'error'));  
		  redirect(base_url().'Search/search');
        }
      }
      else
      {
      	$this->load->view('Search/search');
      } 

		
	}

//search result shows	
	public function search_result(){

		
	  $user_details=$this->session->userdata('userdatas');
	  $template['u_id']=$user_details['id'];
      $data=$_POST;
      $template['result']=$this->search->get_search($data);
    
        if($template['result'])
        {
	      $this->load->view('Search/search_list',$template);
	    }
	    else
        {
          $this->session->set_flashdata('msg', array('message' => 'No Result','class' => 'error'));  
		  redirect(base_url().'Search/search');
        }

	}

//full details of selecting business shows	
	public function view_search_details(){
        
        $user_details=$this->session->userdata('userdatas');
	    $template['user_id']=$user_details['id'];
		$business_id=$this->uri->segment(3);
		$template['data']=$this->search->get_search_result($business_id);
		$template['review']=$this->search->get_reviews($business_id);
		//var_dump($template['data']);
		if($template['data'])
        {
	      $this->load->view('Search/view_search_details',$template);
	    }
	    else
        {
          $this->session->set_flashdata('msg', array('message' => 'No details get','class' => 'error'));  
		  redirect(base_url().'Search/search_result');
        }
	}

//add favourite	
	public function addfavourite(){  
	 
		$user_details=$this->session->userdata('userdatas');
		$user_id=$user_details['id'];
		$business_id=$_GET['fav_id'];
		
		$result=$this->search->addfavourite($business_id,$user_id);

		if($result)
		{
              echo "Success";
		}
		else
		{
             echo "Error";
		}
		
		  
	}
	
	

}
