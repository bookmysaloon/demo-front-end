<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	  public function __construct(){
		parent::__construct();
		
		$this->load->model('User_model','home');
	   
		}

//registration	
	public function index(){

	 if($_POST)
	 {
	 	$data=$_POST;
	 	unset($data['confirm_password']);
		$result=$this->home->user_registration($data);
		
		if($result=="UsernameExist")
		{
          $this->session->set_flashdata('msg', array('message' => 'Username Already Exist','class' => 'error')); 
          redirect(base_url().'User');
		}
		elseif($result=="EmailExist")
		{
        $this->session->set_flashdata('msg', array('message' => 'Email Already Exist','class' => 'error'));  
		redirect(base_url().'User');
		}
		elseif($result=="Success")
		{
        $this->session->set_flashdata('msg', array('message' => 'Successfully Registered','class' => 'success'));  
		redirect(base_url().'User');
		}
		else
		{
        $this->session->set_flashdata('msg', array('message' => 'Error','class' => 'error'));  
		redirect(base_url().'User');
		}		
	  }
	  else
	  {
	  	$this->load->view('Login/home');
	  }	
		
		
	}

//login	
	public function login(){ 
      
	  if($_POST)
	  {
	  	 $data=$_POST;
		 $result=$this->home->login($data);

		    if($result)
		    {

			    $sess_values=array(
				              'id'=>$result->id,
                              'username'=>$result->username
				               );
			
                $this->session->set_userdata('userdatas',$sess_values);
                $this->session->set_flashdata('msg', array('message' => 'Logged in successfully','class' => 'success')); 
          
                if($this->uri->segment(3)=="fav")
                {
                $business_id=$this->uri->segment(4);
                } 
                if($this->uri->segment(3)=="review")
                {
                $review_business_id=$this->uri->segment(4);
                }

               if(isset($business_id))
	            {
	  	          redirect(base_url().'Search/view_search_details/'.$business_id);
	            }
	            elseif(isset($review_business_id))
	            {
	  	          redirect(base_url().'Review/review/'.$review_business_id);
	            }
	            else
	            {
	              redirect(base_url().'Business/business_details');	
	            }	
		    }
		    else
		    {
               $this->session->set_flashdata('msg', array('message' => 'Username or Password Incorrect','class' => 'error')); 
		       redirect(base_url().'User/login');
		    }	
	  }
	  
	  else
	  {
         $this->load->view('Login/login');
	  }
	
	}
	

}
