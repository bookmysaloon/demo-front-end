<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Favourite extends CI_Controller {

	  public function __construct(){
		
		parent::__construct();
		
		
		$this->load->model('Favourite_model','favourite');
		
	
	      if(!$this->session->userdata('userdatas'))
	        {
			redirect(base_url());
		    }
		}
	
//add favourite	
	public function favourite(){
		
		$user_details=$this->session->userdata('userdatas');
		$user_id=$user_details['id'];
		$business_id=$_GET['id'];
		
		$result=$this->favourite->add_favourite($business_id,$user_id);

		if($result)
		{
          echo "Success";
		}
		else
		{
         echo "Error";
		}
	}

//shows favourite list	
	public function favourite_list(){
       
        $user_details=$this->session->userdata('userdatas');
		$user_id=$user_details['id'];
        $template['fav_list']=$this->favourite->favourite_list($user_id);
		$this->load->view('Favourite/favourite_list',$template);
	
	}

//unfavourite	
	public function unfavourite(){

		$user_details=$this->session->userdata('userdatas');
		$user_id=$user_details['id'];
		$fav_id=$_GET['fav_id'];
		
		$result=$this->favourite->delete_favourite($fav_id,$user_id);
      
		if($result)
		{
          echo "Success";
		}
		else
		{
         echo "Error";
		}
	}
	
	 


}
