<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Business extends CI_Controller {

	  public function __construct(){
		parent::__construct();
		
		
		$this->load->model('Business_model','business');
		
	
	      if(!$this->session->userdata('userdatas'))
	        {
			redirect(base_url());
		    }
	   }
	
//add business	
	public function add_business(){


	 if($_POST)
	 {
		$user_details=$this->session->userdata('userdatas');
		$user_id=$user_details['id'];
		$data=$_POST;
		$data['created_by']=$user_id;
		$result=$this->business->add_business($data);

		if($result)
		{
         $this->session->set_flashdata('msg', array('message' => 'Successfully Added','class' => 'success')); 
         redirect(base_url().'Business/add_business');
		}
		else
		{
        $this->session->set_flashdata('msg', array('message' => 'Error','class' => 'error'));
        redirect(base_url().'Business/add_business');  
		}

	  }
	  else
	  {
	  	$this->load->view('Business/add_business');
	  }	
		
		
	}

//show business details 	
	public function business_details(){ 

		$user_details=$this->session->userdata('userdatas');
		$user_id=$user_details['id'];
		$template['data']=$this->business->details_business($user_id);
		$this->load->view('Business/business_details',$template);
	}

//edit business details	
	public function edit_business(){

		$user_details=$this->session->userdata('userdatas');
		$user_id=$user_details['id'];
		$id = $this->uri->segment(3);
		
		if($_POST)
		 {
          
         $value=$this->input->post();
         $value['created_by']=$user_id;
         $result=$this->business->business_update_result($value,$id);
          
            if($result)
		     {
               $this->session->set_flashdata('item', array('message' => 'Successfully Updated','class' => 'success')); 
		       redirect(base_url().'Business/business_details');
		     }
		    else
		     {
               $this->session->set_flashdata('item', array('message' => 'Error','class' => 'error'));  
		       redirect(base_url().'Business/business_details');
		     }	
		 
		 }
		 else
		 {
		  $template['data']=$this->business->business_update($id,$user_id);

		  //var_dump($template['data']);
		    if($template['data'])
		     {
		     $this->load->view('Business/edit_business',$template);
		     }
		    else
		     {
              $this->session->set_flashdata('item', array('message' => 'Cannot Access','class' => 'error'));
		      redirect(base_url().'Business/business_details');
		     } 
		 }
   	}

//delete business information   	
   	public function delete_business(){

		$id=$this->uri->segment(3);
		$result=$this->business->business_delete($id);

		if($result)
		   {
             $this->session->set_flashdata('item', array('message' => 'Successfully Deleted','class' => 'success')); 
		     redirect(base_url().'Business/business_details');
		   }
		   else
		   {
             $this->session->set_flashdata('item', array('message' => 'Error','class' => 'error'));  
		     redirect(base_url().'Business/business_details');
		   }	

	}
   		

	 


}
