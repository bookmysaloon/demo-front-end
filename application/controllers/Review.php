<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Review extends CI_Controller {

	  public function __construct(){
		parent::__construct();
		
		
		$this->load->model('Review_model','review');
		
		/*if(!$this->session->userdata('userdatas'))
	        {
			redirect(base_url().'User/login');
		    }*/
	
	      
	   }
	
//add rating and review	
	public function review(){

		$business_id=$this->uri->segment(3);
		$user_details=$this->session->userdata('userdatas');
		$user_id=$user_details['id'];

	   if($_POST){
         
         
         $data=$this->input->post();
         $data['business_id']=$business_id;
         $data['user_id']=$user_id;
         $result=$this->review->addreview($data);
          
          if($result){
            
            $this->session->set_flashdata('msg', array('message' => 'Successfully Added','class' => 'success')); 
            redirect(base_url().'Review/review');
          }
          else{
          	
          	$this->session->set_flashdata('msg', array('message' => 'Error','class' => 'error'));
            redirect(base_url().'Review/review');  

          }

	   }
	   else{

	   	   $review_deatils=$this->review->get_review_details($business_id,$user_id);
	  
	   	   if($review_deatils=="Novalue")
	   	    {
	   	  	  $template['review']="";
	   	  	  $this->load->view('Reviews/review',$template);
	   	    }
	   	    else
	   	    {
	   	  	  $template['review']=$review_deatils;
	   	  	  $this->load->view('Reviews/review',$template);
	   	    }
	     
	 		
	    }	

      
	}
   		

	 


}
